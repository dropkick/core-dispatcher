<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Formattable\FormattableString;
use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchInterface;

/**
 * Class CallableResolver.
 *
 * A callable resolver that uses other callable resolvers to process
 * the matched routes.
 */
class CallableResolver implements CallableResolverInterface {

  /**
   * The list of callable resolvers.
   *
   * @var \Dropkick\Core\Dispatcher\CallableResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Add a resolver to the list of resolvers.
   *
   * @param \Dropkick\Core\Dispatcher\CallableResolverInterface $resolver
   *   A callable resolver object.
   *
   * @return static
   *   The callable resolver object.
   */
  public function addResolver(CallableResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function canResolve($definition, MatchInterface $match, ContextInterface $context) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->canResolve($definition, $match, $context)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCallable($definition, MatchInterface $match, ContextInterface $context) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->canResolve($definition, $match, $context)) {
        return $resolver->getCallable($definition, $match, $context);
      }
    }
    throw new \InvalidArgumentException(
      FormattableString::create(
        'The callable for URI "{{ route }}" is not resolved.',
        ['route' => $match->getRouteName()]
      )
    );
  }

}
