<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchInterface;

/**
 * Interface DispatcherInterface.
 *
 * A dispatcher is used for checking that the matched route can be called,
 * and then provides the way for that matched route to be called with the
 * given information.
 */
interface DispatcherInterface {

  /**
   * Confirm the dispatcher can handle the route.
   *
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The matched route.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The request context.
   *
   * @return bool
   *   Confirmation this dispatcher can handle the matched route.
   */
  public function canDispatch(MatchInterface $match, ContextInterface $context);

  /**
   * Dispatches the route request.
   *
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The matched route.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The request context.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response object.
   */
  public function dispatch(MatchInterface $match, ContextInterface $context);

}
