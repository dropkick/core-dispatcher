<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Formattable\FormattableString;
use Dropkick\Core\Invokable\Reflector;
use Dropkick\Core\Invokable\ReflectorInterface;
use Dropkick\Core\Invokable\ResolverInterface;
use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchInterface;
use Dropkick\Core\Dispatcher\Instantiator\CallableResolver;

/**
 * Class Dispatcher.
 *
 * A generic implementation of DispatcherInterface, which uses the route
 * option '_controller' to define the callable dispatcher.
 */
class Dispatcher implements DispatcherInterface {

  /**
   * The callable resolver.
   *
   * @var \Dropkick\Core\Dispatcher\CallableResolverInterface
   */
  protected $callableResolver;

  /**
   * The argument resolver.
   *
   * @var \Dropkick\Core\Dispatcher\ArgumentResolverInterface
   */
  protected $argumentResolver;

  /**
   * Dispatcher constructor.
   *
   * @param \Dropkick\Core\Dispatcher\CallableResolverInterface $callableResolver
   *   The callable resolver object.
   * @param \Dropkick\Core\Dispatcher\ArgumentResolverInterface $argumentResolver
   *   The argument resolver object.
   */
  public function __construct(
    CallableResolverInterface $callableResolver,
    ArgumentResolverInterface $argumentResolver
  ) {
    $this->callableResolver = $callableResolver;
    $this->argumentResolver = $argumentResolver;
  }

  /**
   * {@inheritdoc}
   */
  public function canDispatch(MatchInterface $match, ContextInterface $context) {
    return $match->getRouteObject()->hasOption('_controller');
  }

  /**
   * {@inheritdoc}
   */
  public function dispatch(MatchInterface $match, ContextInterface $context) {
    $route = $match->getRouteObject();
    $callable = $this->callableResolver->getCallable($route->getOption('_controller'), $match, $context);

    // Get the matched parameters.
    $parameters = $match->getParameters();

    // Check the callable is actually callable.
    if (!is_callable($callable)) {
      throw new \DomainException(
        FormattableString::create(
          'The callable for Route "{{ route }}" is not callable.',
          ['route' => $match->getRouteName()]
        )
      );
    }

    $reflector = new Reflector();
    $resolver = new CallableResolver(
      $this->argumentResolver,
      $match,
      $context,
      $parameters
    );

    $arguments = $this->getArguments($callable, $reflector, $resolver);

    return call_user_func_array($callable, $arguments);
  }

  /**
   * Return the callable arguments.
   *
   * @param $callable
   *   The callable.
   *
   * @return array
   *   The argument values.
   *
   * @throws \LogicException
   */
  protected function getArguments(
    $callable,
    ReflectorInterface $reflector,
    ResolverInterface $resolver
  ) {
    $arguments = [];

    // Map all the constructor arguments using the resolver.
    foreach ($reflector->getArguments($callable) as $arg) {
      if ($resolver->applies($arg)) {
        $arguments[] = $resolver->getValue($arg);
      }
      elseif (!$arg->hasDefault()) {
        throw new \LogicException(
          FormattableString::create(
            'Argument "{{ argument }} does not provide a value.',
            ['argument' => $arg->getName()]
          )
        );
      }
    }
    return $arguments;
  }


}
