<?php

namespace Dropkick\Core\Dispatcher\Instantiator;

use Dropkick\Core\Dispatcher\Argument;
use Dropkick\Core\Dispatcher\ArgumentResolverInterface;
use Dropkick\Core\Invokable\ArgumentInterface;
use Dropkick\Core\Invokable\ResolverInterface;
use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchInterface;

/**
 * Class CallableResolver
 *
 * Allow a callable dispatch to resolve the arguments for dispatching.
 */
class CallableResolver implements ResolverInterface {

  /**
   * The arguments.
   *
   * @var array
   */
  protected $arguments = [];

  /**
   * The argument resolver factory.
   *
   * @var \Dropkick\Core\Dispatcher\ArgumentResolverInterface
   */
  protected $resolver;

  /**
   * The match object.
   *
   * @var \Dropkick\Core\Router\MatchInterface
   */
  protected $match;

  /**
   * The context object.
   *
   * @var \Dropkick\Core\Router\ContextInterface
   */
  protected $context;

  /**
   * CallableResolver constructor.
   *
   * @param \Dropkick\Core\Dispatcher\ArgumentResolverInterface $resolver
   *   The argument resolver object.
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The match object.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The context object.
   * @param array $arguments
   *   The arguments.
   */
  public function __construct(
    ArgumentResolverInterface $resolver,
    MatchInterface $match,
    ContextInterface $context,
    array $arguments = []
  ) {
    $this->arguments = $arguments;
    $this->resolver = $resolver;
    $this->match = $match;
    $this->context = $context;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(ArgumentInterface $argument) {
    if (!$argument->getType()) {
      return $argument->hasDefault() || array_key_exists($argument->getName(), $this->arguments);
    }
    return $this->resolver->canResolve(
      $this->getArgument($argument->getType(), $argument->getName()),
      $this->match,
      $this->context
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ArgumentInterface $argument) {
    if (!$argument->getType()) {
      if (array_key_exists($argument->getName(), $this->arguments)) {
        return $this->arguments[$argument->getName()];
      }
      return $argument->getDefault();
    }
    return $this->resolver->getArgument(
      $this->getArgument($argument->getType(), $argument->getName()),
      $this->match,
      $this->context
    );
  }

  /**
   * Get the argument as a class.
   *
   * @param string $type
   *   The type hint of the argument.
   * @param string $name
   *   The name of the argument.
   *
   * @return \Dropkick\Core\Dispatcher\ArgumentInterface
   *   An argument instance.
   */
  protected function getArgument($type, $name) {
    return new Argument($type, $name);
  }
}
