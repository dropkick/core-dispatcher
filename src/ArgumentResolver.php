<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Formattable\FormattableString;
use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchInterface;

/**
 * Class ArgumentResolver.
 *
 * An argument resolver that uses other argument resolvers to convert
 * arguments.
 */
class ArgumentResolver implements ArgumentResolverInterface {

  /**
   * The argument resolvers.
   *
   * @var \Dropkick\Core\Dispatcher\ArgumentResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Add a resolver.
   *
   * @param \Dropkick\Core\Dispatcher\ArgumentResolverInterface $resolver
   *   An argument resolver.
   *
   * @return static
   *   The argument resolver object.
   */
  public function addResolver(ArgumentResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function canResolve(ArgumentInterface $argument, MatchInterface $match, ContextInterface $context) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->canResolve($argument, $match, $context)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument(ArgumentInterface $argument, MatchInterface $match, ContextInterface $context) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->canResolve($argument, $match, $context)) {
        return $resolver->getArgument($argument, $match, $context);
      }
    }
    throw new \InvalidArgumentException(
      FormattableString::create(
        'Argument "{{ argument }}" was not resolvable for "{{ route }}".',
        ['argument' => $argument->getName(), 'route' => $match->getRouteName()]
      )
    );
  }

}
