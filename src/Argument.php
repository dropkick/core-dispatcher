<?php

namespace Dropkick\Core\Dispatcher;

/**
 * Class Argument.
 *
 * A generic implementation of ArgumentInterface.
 */
class Argument implements ArgumentInterface {

  /**
   * The type hint.
   *
   * @var string
   */
  protected $type;

  /**
   * The name.
   *
   * @var string
   */
  protected $name;

  /**
   * Argument constructor.
   *
   * @param string $type
   *   The type hint of the argument.
   * @param string $name
   *   The name of the argument.
   */
  public function __construct($type, $name) {
    $this->type = $type;
    $this->name = $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

}
