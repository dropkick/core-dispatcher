<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchInterface;

/**
 * Interface ArgumentResolverInterface.
 *
 * This converts arguments or parameters into the appropriate php value, that
 * will then be passed by the dispatcher to the callable.
 */
interface ArgumentResolverInterface {

  /**
   * Confirm the resolve can handle the argument.
   *
   * @param \Dropkick\Core\Dispatcher\ArgumentInterface $argument
   *   The argument to resolve.
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The matched route.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The request context.
   *
   * @return bool
   *   Confirmation this argument resolver can handle the argument.
   */
  public function canResolve(ArgumentInterface $argument, MatchInterface $match, ContextInterface $context);

  /**
   * Get the conversion of the argument.
   *
   * @param \Dropkick\Core\Dispatcher\ArgumentInterface $argument
   *   The argument to resolve.
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The matched route.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The request context.
   *
   * @return mixed
   *   The php value representing the argument.
   */
  public function getArgument(ArgumentInterface $argument, MatchInterface $match, ContextInterface $context);

}
