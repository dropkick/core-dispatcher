<?php

namespace Dropkick\Core\Dispatcher;

/**
 * Interface ArgumentInterface.
 *
 * An argument for a callable, includes the type hint and the name of the
 * parameter.
 */
interface ArgumentInterface {

  /**
   * Get the type of the argument.
   *
   * @return string
   *   The type hint of the argument.
   */
  public function getType();

  /**
   * Get the name of the argument.
   *
   * @return string
   *   The name of the argument.
   */
  public function getName();

}
