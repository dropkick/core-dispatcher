<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchInterface;

/**
 * Interface CallableResolverInterface.
 *
 * Converts a route callable definition into a callable.
 */
interface CallableResolverInterface {

  /**
   * Confirm the resolver can handle the definition.
   *
   * @param mixed $definition
   *   The definition as provided by the route.
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The matched route.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The request context.
   *
   * @return bool
   *   Confirmation the object can handle the callable conversion.
   */
  public function canResolve($definition, MatchInterface $match, ContextInterface $context);

  /**
   * Get the callable based on the definition and parameters.
   *
   * @param mixed $definition
   *   The definition used for the callable.
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The matched route.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The request context.
   *
   * @return callable|null
   *   The callable, or NULL if incapable of the conversion.
   */
  public function getCallable($definition, MatchInterface $match, ContextInterface $context);

}
