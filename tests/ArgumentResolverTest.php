<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Router\ContextFactory;
use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchFactory;
use Dropkick\Core\Router\MatchInterface;
use Dropkick\Core\Router\Route;
use Dropkick\Core\Router\RouteInterface;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;

class ArgumentResolverTest extends TestCase {

  /**
   * @var \Dropkick\Core\Router\MatchInterface
   */
  protected $match;

  /**
   * @var \Dropkick\Core\Router\ContextInterface
   */
  protected $context;

  /**
   * @var \Dropkick\Core\Router\RouteInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $route;

  /**
   * @var \Psr\Http\Message\RequestInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $request;

  /**
   * Setup the tests.
   */
  public function setUp(): void {
    $this->route = new Route(['path' => 'path']);
    $this->request = new Request('GET', 'http://example.com/');
    $this->match = (new MatchFactory())->createMatch('route', $this->route, [], []);
    $this->context = (new ContextFactory())->getRequestContext($this->request);
  }

  /**
   * Test the behaviour with no resolvers loaded.
   */
  public function testNoResolvers() {
    $argument = new Argument('bool', 'test');
    $resolver = new ArgumentResolver();

    $this->assertFalse($resolver->canResolve($argument, $this->match, $this->context));

    $this->expectException(\InvalidArgumentException::class);
    $resolver->getArgument($argument, $this->match, $this->context);
  }

  public function testCannotResolve() {
    $argument = new Argument('bool', 'test');
    $resolver = new ArgumentResolver();
    $resolver->addResolver(new TestArgumentResolver());

    // Should fail the canResolve.
    $this->assertFalse($resolver->canResolve($argument, $this->match, $this->context));

    // Should pass the argument conversion.
    $this->expectException(\InvalidArgumentException::class);
    $resolver->getArgument($argument, $this->match, $this->context);
  }

  public function testResolve() {
    $argument = new Argument('string', 'test');
    $resolver = new ArgumentResolver();
    $resolver->addResolver(new TestArgumentResolver());

    // Should fail the canResolve.
    $this->assertTrue($resolver->canResolve($argument, $this->match, $this->context));

    // Should pass the argument conversion.
    $result = $resolver->getArgument($argument, $this->match, $this->context);
    $this->assertEquals('test', $result);
  }

}

class TestArgumentResolver implements ArgumentResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function canResolve(ArgumentInterface $argument, MatchInterface $match, ContextInterface $context) {
    return $argument->getType() !== 'bool';
  }


  /**
   * {@inheritdoc}
   */
  public function getArgument(ArgumentInterface $argument, MatchInterface $match, ContextInterface $context) {
    return $argument->getName();
  }

}