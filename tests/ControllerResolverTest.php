<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Router\ContextFactory;
use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchFactory;
use Dropkick\Core\Router\MatchInterface;
use Dropkick\Core\Router\Route;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class ControllerResolverTest extends TestCase {

  /**
   * @var \Dropkick\Core\Router\RouteInterface
   */
  protected $route;

  /**
   * @var \Psr\Http\Message\RequestInterface
   */
  protected $request;

  /**
   * @var \Dropkick\Core\Router\MatchInterface
   */
  protected $match;

  /**
   * @var \Dropkick\Core\Router\ContextInterface
   */
  protected $context;

  /**
   * Setup the tests.
   */
  public function setUp(): void {
    $this->route = new Route(['path' => 'path']);
    $this->request = new Request('GET', 'http://example.com/base');
    $this->match = (new MatchFactory())->createMatch('route', $this->route, [], []);
    $this->context = (new ContextFactory())->getRequestContext($this->request);
  }

  public function testNoResolvers() {
    $definition = 'resolvable';
    $resolver = new CallableResolver();

    $this->assertFalse($resolver->canResolve($definition, $this->match, $this->context));

    $this->expectException(\InvalidArgumentException::class);
    $resolver->getCallable($definition, $this->match, $this->context);
  }

  public function testCannotResolve() {
    $definition = 'irresolvable';
    $resolver = new CallableResolver();
    $resolver->addResolver(new TestCallableResolver());

    $this->assertFalse($resolver->canResolve($definition, $this->match, $this->context));

    $this->expectException(\InvalidArgumentException::class);
    $resolver->getCallable($definition, $this->match, $this->context);
  }

  public function testResolve() {
    $definition = 'resolvable';
    $resolver = new CallableResolver();
    $resolver->addResolver(new TestCallableResolver());

    $this->assertTrue($resolver->canResolve($definition, $this->match, $this->context));

    $result = $resolver->getCallable($definition, $this->match, $this->context);
    $this->assertEquals($definition, $result);
  }

}

class TestCallableResolver implements CallableResolverInterface {

  public function canResolve($definition, MatchInterface $match, ContextInterface $context) {
    return is_array($definition) || is_string($definition) && $definition !== 'irresolvable';
  }

  public function getCallable($definition, MatchInterface $match, ContextInterface $context) {
    return $definition;
  }

}
