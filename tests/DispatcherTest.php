<?php

namespace Dropkick\Core\Dispatcher;

use Dropkick\Core\Router\ContextFactory;
use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\MatchFactory;
use Dropkick\Core\Router\MatchInterface;
use Dropkick\Core\Router\Route;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class DispatcherTest extends TestCase {

  /**
   * @var \Dropkick\Core\Router\RouteInterface
   */
  protected $route;

  /**
   * @var \Psr\Http\Message\RequestInterface
   */
  protected $request;

  /**
   * @var \Dropkick\Core\Dispatcher\CallableResolverInterface
   */
  protected $controller;

  /**
   * @var \Dropkick\Core\Dispatcher\ArgumentResolverInterface
   */
  protected $argument;

  /**
   * Setup the tests.
   */
  public function setUp(): void {
    $this->route = new Route(['path' => 'path']);
    $this->request = new Request('GET', 'http://example.com/base');

    $this->controller = new TestDispatchCallableResolver;
    $this->argument = new TestDispatchArgumentResolver;
  }

  protected function getMatch() {
    return (new MatchFactory())->createMatch('route', $this->route, [], []);
  }

  protected function getContext() {
    return (new ContextFactory())->getRequestContext($this->request);
  }

  public function testNotResolvable() {
    $match = $this->getMatch();
    $context = $this->getContext();
    
    $dispatcher = new Dispatcher($this->controller, $this->argument);

    $this->assertFalse($dispatcher->canDispatch($match, $context));

    $this->expectException(\DomainException::class);
    $dispatcher->dispatch($match, $context);
  }

  public function testUninvokable() {
    $this->route = $this->route->withOptions([
      '_controller' => TestUninvokableController::class
    ]);
    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch();
    $context = $this->getContext();

    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $this->expectException(\DomainException::class);
    $dispatcher->dispatch($match, $context);
  }


  public function testInvokable() {
    $this->route = $this->route->withOptions([
      '_controller' => TestInvokableController::class
    ]);

    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch();
    $context = $this->getContext();

    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $result = $dispatcher->dispatch($match, $context);
    $this->assertEquals('invokable', $result);
  }

  public function testMethodCallable() {
    $this->route = $this->route->withOptions([
      '_controller' => [TestUninvokableController::class, 'publicMethod']
    ]);

    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch()->withParameters(['test' => 'paramValue']);
    $context = $this->getContext();
    
    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $result = $dispatcher->dispatch($match, $context);
    $this->assertEquals('paramValue', $result);
  }

  public function testMethodDefault() {
    $this->route = $this->route->withOptions([
      '_controller' => [TestUninvokableController::class, 'publicMethodDefault']
    ]);

    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch()->withParameters(['test' => 'paramValue']);
    $context = $this->getContext();

    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $result = $dispatcher->dispatch($match, $context);
    $this->assertEquals('paramValue', $result);
  }

  public function testMethodDefaultValue() {
    $this->route = $this->route->withOptions([
      '_controller' => [TestUninvokableController::class, 'publicMethodDefault']
    ]);

    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch()->withParameters(['test' => 'paramValue', 'item' => 'paramValue']);
    $context = $this->getContext();

    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $result = $dispatcher->dispatch($match, $context);
    $this->assertEquals('paramValueparamValue', $result);
  }

  public function testMethodDefaultValueUndefined() {
    $this->route = $this->route->withOptions([
      '_controller' => [TestUninvokableController::class, 'publicMethodDefaultUndefined']
    ]);

    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch()->withParameters(['test' => 'paramValue', 'item' => 'paramValue']);
    $context = $this->getContext();

    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $this->expectException(\LogicException::class);
    $result = $dispatcher->dispatch($match, $context);
  }

  public function testFunctionCallable() {
    $this->route = $this->route->withOptions([
      '_controller' => 'Dropkick\Core\Dispatcher\testFunctionDispatch'
    ]);

    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch()->withParameters(['test' => 'paramValue']);
    $context = $this->getContext();

    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $result = $dispatcher->dispatch($match, $context);
    $this->assertEquals('paramValue', $result);
  }

  public function testClosureCallable() {
    $this->route = $this->route->withOptions([
      '_controller' => function(TestUninvokableController $controller, $test) {
        return $test;
      }
    ]);

    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch()->withParameters(['test' => 'paramValue']);
    $context = $this->getContext();

    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $result = $dispatcher->dispatch($match, $context);
    $this->assertEquals('paramValue', $result);
  }
  
  public function testInvalidCallable() {
    $this->route = $this->route->withOptions([
      '_controller' => new TestCallableController()
    ]);


    $dispatcher = new Dispatcher($this->controller, $this->argument);
    $match = $this->getMatch()->withParameters(['test' => 'paramValue']);
    $context = $this->getContext();

    $this->assertTrue($dispatcher->canDispatch($match, $context));

    $this->expectException(\Exception::class);
    $result = $dispatcher->dispatch($match, $context);
  }

}

class TestDispatchArgumentResolver implements ArgumentResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function canResolve(ArgumentInterface $argument, MatchInterface $match, ContextInterface $context) {
    return $argument->getType() !== 'bool';
  }


  /**
   * {@inheritdoc}
   */
  public function getArgument(ArgumentInterface $argument, MatchInterface $match, ContextInterface $context) {
    $type = $argument->getType();
    if (class_exists($type)) {
      return new $type();
    }
    return $argument->getName();
  }

}


class TestDispatchCallableResolver implements CallableResolverInterface {

  public function canResolve($definition, MatchInterface $match, ContextInterface $context) {
    if (is_array($definition)) {
      return class_exists($definition[0]);
    }
    if ($definition instanceof \Closure || $definition instanceof TestCallableController) {
      return TRUE;
    }
    return class_exists($definition);
  }

  public function getCallable($definition, MatchInterface $match, ContextInterface $context) {
    if (is_array($definition)) {
      return [new $definition[0](), $definition[1]];
    }
    if ($definition instanceof \Closure) {
      return $definition;
    }
    // This is primarily to test a callable that shouldn't be handled currently.
    if ($definition instanceof TestCallableController) {
      return TestCallableController::class . '::randomMethod';
    }
    if (class_exists($definition)) {
      return new $definition();
    }
    return $definition;
  }

}

class TestUninvokableController {
  public function publicMethod(TestUninvokableController $controller, $test) {
    return $test;
  }

  public function publicMethodDefault(TestInvokableController $controller, $test, $item = NULL) {
    return $test . $item;
  }

  public function publicMethodDefaultUndefined(TestInvokableController $controller, $test, $undefined, $item = NULL) {
    return $test;
  }

  protected function protectedMethod(TestInvokableController $controller, $test) {
    return $test;
  }
}

class TestCallableController {
  public static function __callStatic($a, $b) {
    throw new \Exception('This should not be called');
  }
}

class TestInvokableController {
  public function __invoke() {
    return 'invokable';
  }
}

function testFunctionDispatch(TestUninvokableController $controller, $test) {
  return $test;
}

